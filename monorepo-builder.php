<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\ComposerJsonManipulator\ValueObject\ComposerJsonSection;
use Symplify\MonorepoBuilder\ValueObject\Option;
use Symplify\MonorepoBuilder\Release\ReleaseWorker\AddTagToChangelogReleaseWorker;
use Symplify\MonorepoBuilder\Release\ReleaseWorker\PushNextDevReleaseWorker;
use Symplify\MonorepoBuilder\Release\ReleaseWorker\PushTagReleaseWorker;
use Symplify\MonorepoBuilder\Release\ReleaseWorker\SetCurrentMutualDependenciesReleaseWorker;
use Symplify\MonorepoBuilder\Release\ReleaseWorker\SetNextMutualDependenciesReleaseWorker;
use Symplify\MonorepoBuilder\Release\ReleaseWorker\TagVersionReleaseWorker;
use Symplify\MonorepoBuilder\Release\ReleaseWorker\UpdateBranchAliasReleaseWorker;
use Symplify\MonorepoBuilder\Release\ReleaseWorker\UpdateReplaceReleaseWorker;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();
    // for "merge" command
    $parameters->set(Option::DATA_TO_APPEND, [
        'require-dev' => [
            'phpunit/phpunit' => '^9.5',
        ],
    ]);

    $parameters->set(Option::DIRECTORIES_TO_REPOSITORIES, [
        'packages/Ajax' => 'git@bitbucket.org:bravissimo_agency/wp-next-ajax.git',
        'packages/Console' => 'git@bitbucket.org:bravissimo_agency/wp-next-console.git',
        'packages/Core' => 'git@bitbucket.org:bravissimo_agency/wp-next-core.git',
        'packages/Hook' => 'git@bitbucket.org:bravissimo_agency/wp-next-hook.git',
        'packages/Routing' => 'git@bitbucket.org:bravissimo_agency/wp-next-routing.git',
        'packages/Support' => 'git@bitbucket.org:bravissimo_agency/wp-next-support.git',
        'packages/View' => 'git@bitbucket.org:bravissimo_agency/wp-next-view.git',
    ]);

    $services = $containerConfigurator->services();

    # release workers - in order to execute
    $services->set(UpdateReplaceReleaseWorker::class);
    $services->set(SetCurrentMutualDependenciesReleaseWorker::class);
    $services->set(AddTagToChangelogReleaseWorker::class);
    $services->set(TagVersionReleaseWorker::class);
    $services->set(PushTagReleaseWorker::class);
    $services->set(SetNextMutualDependenciesReleaseWorker::class);
    $services->set(UpdateBranchAliasReleaseWorker::class);
    $services->set(PushNextDevReleaseWorker::class);
};
